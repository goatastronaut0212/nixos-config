{ pkgs, ... }:

{
  users.users.astronaut = {
    isNormalUser = true;
    description = "astronaut";
    extraGroups = [ "audio" "input" "libvirtd" "networkmanager" "video" "postgres" "wheel" ];
  };
}
