{ pkgs, ... }:

{
  environment = {
    systemPackages = with pkgs; [
      # Lang
      jdk17
      nodejs

      appimage-run
      dmidecode

      # Tools
      cloc
      efibootmgr
      usbutils

      git
      htop
      unzipNLS
      virt-manager
      pciutils
      pulsemixer
      xorg.xbacklight
      xz
      zip
      zstd

      steam-run
    ];
  };
}
