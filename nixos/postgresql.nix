{ pkgs, ... }:

{
  services.postgresql = {
    enable = false;
    package = pkgs.postgresql_15;
    port = 5432;
  };
}
