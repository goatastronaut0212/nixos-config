{ pkgs, ... }:

{
  fonts.packages = with pkgs; [
    barlow
    corefonts
    font-awesome
    (nerdfonts.override { fonts = [ "JetBrainsMono" "Terminus" "Iosevka" ]; })
    noto-fonts
    noto-fonts-cjk
    noto-fonts-emoji
  ];
}
