{
  services.xserver = {
    # Keymap in X11
    layout = "us";
    xkbVariant = "";

    # Enable the X11 window system
    enable = true;
    autorun = true;

    videoDrivers = [
      "intel"
    ];

    # touchpad support
    libinput.enable = true;

    displayManager.startx.enable = true;
    windowManager.qtile.enable = true;
  };
}
