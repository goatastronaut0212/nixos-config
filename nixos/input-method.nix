{ pkgs, ... }:

let
  bamboo = pkgs.callPackage ../pkgs/ibus-bamboo.nix {};
in
{
  i18n.inputMethod = {
    enabled = "ibus";
    # ibus.engines = with pkgs.unstable.ibus-engines; [
    ibus.engines = [
      bamboo
    ];
  };
}
