{ config, lib, pkgs, ... }:

{
  boot = {
    initrd = {
      availableKernelModules = [ "xhci_pci" "ahci" "nvme" "usbhid" "usb_storage" "sd_mod" ];
      kernelModules = [ ];
    };
    kernelModules = [ "kvm-intel" ];
    extraModulePackages = [ ];
  };

  fileSystems."/" =
    { device = "/dev/disk/by-uuid/d952c4b4-395b-4619-bcec-6fb912e0e98e";
      fsType = "ext4";
    };

  fileSystems."/efi" =
    { device = "/dev/disk/by-uuid/5825-4B3A";
      fsType = "vfat";
      options = [ "fmask=0077" "dmask=0077" "defaults" ];
    };

  swapDevices =
    [ { device = "/dev/disk/by-uuid/4beb2020-3c08-41c8-ab29-5c48f8c0b316"; }
    ];

  networking.useDHCP = lib.mkDefault true;

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  hardware = {
    enableRedistributableFirmware = false;
    enableAllFirmware = false;
    firmware = with pkgs; [
      linux-firmware
      sof-firmware
    ];
    opengl = {
      enable = true;
      driSupport = true;
      driSupport32Bit = true;
      extraPackages = with pkgs; [
        intel-compute-runtime
      ];
    };
  };
}
