{
  networking = {
    enableIPv6 = true;
    hostName = "nixos"; # Define your hostname.
    networkmanager.enable = true;
    firewall = {
      enable = true;
      allowedTCPPorts = [ 1025 1143 5000 5169 5173 6567 8069 25565 ];
      allowedUDPPorts = [ 1025 1143 5000 5196 5173 6567 8069 25565 ];
    };
  };

  systemd.services.NetworkManager-wait-online.enable = false;
}
