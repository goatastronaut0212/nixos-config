{ pkgs, ... }:

{
  services.cage = {
    enable = true;
    user = "astronaut";
    program = "${pkgs.waydroid}/bin/waydroid show-full-ui";
  };
}
