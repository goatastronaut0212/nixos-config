{
  inputs,
  outputs,
  config,
  pkgs,
  ...
}: {
  imports = [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix

    ./nix-features.nix

    ./boot-loader.nix

    ./environment.nix

    ./networking.nix

    ./fonts.nix

    ./users.nix

    ./sound.nix

    ./tlp.nix # Power management

    ./xorg.nix

    ./input-method.nix

    ./insecure-pkgs.nix

    ./steam.nix

    ./docker.nix
  ];

  nixpkgs = {
    # You can add overlays here
    overlays = [
      # Add overlays your own flake exports (from overlays and pkgs dir):
      #outputs.overlays.additions
      #outputs.overlays.modifications
      outputs.overlays.unstable-packages

      # You can also add overlays exported from other flakes:
      # neovim-nightly-overlay.overlays.default

      # Or define it inline, for example:
      # (final: prev: {
      #   hi = final.hello.overrideAttrs (oldAttrs: {
      #     patches = [ ./change-hello-to-hi.patch ];
      #   });
      # })
    ];

    # Configure your nixpkgs instance
    config = {
      allowUnfree = true;
    };
  };

  # Set your time zone.
  time.timeZone = "Asia/Ho_Chi_Minh";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.utf8";

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable blueman for bluetooth
  hardware.bluetooth.enable = true;
  services.blueman.enable = true;

  # Virtualisation
  virtualisation.libvirtd.enable = true;
  programs.dconf.enable = true;
  virtualisation.lxd.enable = true;
  virtualisation.waydroid.enable = true;
  
  programs.light.enable = true;

  xdg.portal.enable = true;
  xdg.portal.config.common.default = "*";

  services.gvfs.enable = true; # Mount, trash, and other functionalities
  
  services.flatpak.enable = true;

  system.stateVersion = "23.11";
}
