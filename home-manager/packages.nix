{ pkgs, ... }:

{
  home.packages = with pkgs; [
    # Chat
    discord
    element-desktop # Matrix client

    # Development
    blender
    distrobox
    dl-librescore
    godot_4
    insomnia
    nix-prefetch-git
    krita
    musescore
    wpsoffice

    # Internet
    google-chrome
    unstable.microsoft-edge
    unstable.protonvpn-gui
    thunderbird
    unstable.telegram-desktop

    # Games
    unstable.minetest
    unstable.prismlauncher

    # Lang
    jdk8
    go

    # Library
    lzip

    # Language Server
    gopls
    nixd
    nodePackages_latest.typescript-language-server

    # Media player
    obs-studio # Video record and streaming
    mpv  # Video
    unstable.termusic

    # Iconspack
    papirus-icon-theme

    # System Tools
    # Terminal system Tool
    acpid       # A daemon for delivering ACPI events to userspace programs
    eww         # Widget
    fastfetch   # show system info
    feh
    fuzzel      # Menu Wayland
    rofi        # Menu Xorg
    lazygit     # Git TUI
    nvtop
    picom
    waybar
    weston

    # Graphical system tool
    copyq # clipboard manager
    flameshot # take screen shot
    kid3        # Audio tagger
    pcmanfm-qt
    ventoy-full

    # Reader
    xiphos

    # Windows
    wineWowPackages.stable
  ];
}
