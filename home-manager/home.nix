{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  ...
}: 

{
  home = {
    username = "astronaut";
    homeDirectory = "/home/astronaut";

    stateVersion = "23.11";
  };

  imports = [
    ./alacritty.nix
    ./awesome.nix
    ./cursor.nix
    ./eww.nix
    ./firefox.nix
    ./git.nix
    ./hyprland.nix
    ./neovim.nix
    ./packages.nix
    ./qtile.nix
    ./tmux.nix
    ./vscode.nix
    ./waybar.nix
    ./wezterm.nix
    ./zsh.nix
  ];

  programs.home-manager.enable = true;

  systemd.user.startServices = "sd-switch";
}
