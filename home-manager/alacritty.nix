{ pkgs, ... }:

{
  programs.alacritty = with pkgs; {
    enable = true;
    package = unstable.alacritty;
  };

  xdg.configFile = {
    "alacritty" = {
      source = ../dotfiles/alacritty;
      recursive = true;
    };
  };
}
