{
  description = "My desktop flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.11";
    nixpkgs-unstable.url = "github:nixos/nixpkgs/nixos-unstable";
    home-manager = {
      url = "github:nix-community/home-manager/release-23.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = {
    self,
    nixpkgs,
    nixpkgs-unstable,
    home-manager,
  }@inputs:
  let
    inherit (self) outputs;

    # Config settings
    username = "goatastronaut0212";
    system = "x86_64-linux";

    configDir = "./dotfiles";

    pkgs = import nixpkgs { 
      inherit system;
      config.allowUnfree = true;
    };
  in {
    # Custom packages and modifications, exported as overlays
    overlays = import ./overlays { inherit inputs pkgs system;};

    # NixOS configuration
    nixosConfigurations = {
      nixos = nixpkgs.lib.nixosSystem {
        inherit system;
        specialArgs = {inherit inputs outputs;};

        modules = [
          # My configuration
          ./nixos/configuration.nix

          # Using home-manager as module
          home-manager.nixosModules.home-manager
          {
            home-manager = {
              useGlobalPkgs = true;
              useUserPackages = true;
              extraSpecialArgs = {inherit inputs outputs;};
              users.astronaut = import ./home-manager/home.nix;
            };
          }
        ];
      };
    };
  };
}
