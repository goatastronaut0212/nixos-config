{ lib,
  stdenv,
  fetchurl,
  pkgs,
  autoreconfHook
}:

stdenv.mkDerivation rec {
  pname = "capdriver";
  version = "0.1.4-m5";

  src = fetchurl {
    url = "https://github.com/mounaiban/captdriver/archive/refs/tags/${version}.tar.gz";
    hash = "sha256-2UUo025DVA85dGt4qvYmJZHujDGgdvrwokESJeQZ78A=";
  };

  buildInputs = with pkgs; [
    cups
  ];

  nativeBuildInputs = with pkgs; [
    autoreconfHook
    automake
    cups
  ];

  configurePhase = ''
    ./configure prefix=${placeholder "out"}
  '';

  buildPhase = ''
    make
    make ppd
  '';

  meta = with lib; {
    description = "alternative driver for Canon CAPT printers";
    homepage = "https://github.com/mounaiban/captdriver";
    license = lib.licenses.gpl3;
  };
}
