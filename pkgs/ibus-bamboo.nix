{ lib, stdenv
, fetchFromGitHub
, glib
, gettext
, xorg
, pkg-config
, wrapGAppsHook
, gtk3
, go
}:

let
  pname = "ibus-bamboo";
  version = "0.8.4";
  src = fetchFromGitHub {
    owner = "BambooEngine";
    repo = pname;
    rev = "0dff4de27fe65f87a5ddabaa0c23c89ba1d732dc";
    sha256 = "sha256-P09gXuxbD4RJcXvgnRyFgSxt6NEXfpXJDPzl50ZtAxM=";
  };
in
stdenv.mkDerivation rec {
  inherit pname version src;

  nativeBuildInputs = [
    gettext
    pkg-config
    wrapGAppsHook
    go
  ];

  buildInputs = [
    glib
    gtk3
    xorg.libX11
    xorg.xorgproto
    xorg.libXtst
    xorg.libXi
  ];

  configurePhase = ''
    export GOCACHE="$TMPDIR/go-cache"
    export PREFIX=${placeholder "out"}
    sed -i "s,/usr,$out," data/bamboo.xml
  '';

  buildPhase = ''
    make build
  '';

  checkPhase = ''
    make t
  '';

  makeFlags = [
    "PREFIX=${placeholder "out"}"
  ];

  meta = with lib; {
    isIbusEngine = true;
    description = "A Vietnamese IME for IBus";
    homepage = "https://github.com/BambooEngine/ibus-bamboo";
    license = licenses.gpl3;
    platforms = platforms.linux;
  };
}
