vim.opt.number = true
vim.opt.cursorline = true

vim.cmd("syntax enable")

-- vim.cmd("set background=light")
vim.cmd("set background=dark")
vim.cmd("colorscheme catppuccin-frappe")
-- vim.cmd("colorscheme strawberry-light")
