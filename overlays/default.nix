{ inputs, ...}:

{
  /*
  modifications = final: prev: {
    ibusWayland = prev.ibus {

    }
    # example = prev.example.overrideAttrs (oldAttrs: rec {
    # ...
    # });
    };
  */

  unstable-packages = final: _prev: {
    unstable = import inputs.nixpkgs-unstable {
      system = final.system;
      config.allowUnfree = true;
    };
  };
}
